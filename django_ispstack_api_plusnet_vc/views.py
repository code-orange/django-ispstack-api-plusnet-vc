from base64 import b64decode
from pprint import pprint

import xmltodict
from django.views.decorators.csrf import csrf_exempt
from lxml import etree
from spyne.application import Application
from spyne.decorator import rpc
from spyne.model.primitive import AnyDict
from spyne.model.primitive import Unicode
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from spyne.service import ServiceBase

from django_voipstack_actions.django_voipstack_actions.models import *


def on_method_return_string(ctx):
    ctx.out_string[0] = ctx.out_string[0].replace(
        b"tns:submitXmlMessageReturn>", b"submitXmlMessageReturn>"
    )


class SubmitService(ServiceBase):
    @rpc(
        Unicode,
        Unicode,
        Unicode,
        _returns=AnyDict,
        _out_variable_name="submitXmlMessageReturn",
    )
    def submitXmlMessage(ctx, partnerno, xml, signature):
        namespaces = {"x": "http://partner.qsc.de/xchange/voip-4.0"}

        transaction = False

        qsc_xml_root = etree.fromstring(b64decode(xml))

        partnerno = qsc_xml_root.xpath("x:header/x:partnerno", namespaces=namespaces)
        if len(partnerno):
            pprint(partnerno[0].text)

        referenceNoPartner = qsc_xml_root.xpath(
            "x:header/x:referenceNoPartner", namespaces=namespaces
        )
        if len(referenceNoPartner):
            pprint(referenceNoPartner[0].text)

        transactionNo = qsc_xml_root.xpath(
            "x:header/x:transactionNo", namespaces=namespaces
        )
        if len(transactionNo):
            transaction = VoipJob.objects.using("voipstack").get(
                id=int(transactionNo[0].text)
            )
        else:
            print("no transct id")
            exit()

        acceptance = qsc_xml_root.xpath("x:acceptance", namespaces=namespaces)
        if len(acceptance):
            transaction.job_state = VoipJobCarrierStates.objects.using("voipstack").get(
                id=9
            )
            transaction.save(using="voipstack")

        refusal = qsc_xml_root.xpath("x:refusal", namespaces=namespaces)
        if len(refusal):
            transaction.job_state = VoipJobCarrierStates.objects.using("voipstack").get(
                id=6
            )
            transaction.save(using="voipstack")

        interimconfirmation = qsc_xml_root.xpath(
            "x:interimconfirmation", namespaces=namespaces
        )
        if len(interimconfirmation):
            transaction.job_state = VoipJobCarrierStates.objects.using("voipstack").get(
                id=3
            )
            transaction.save(using="voipstack")

        information = qsc_xml_root.xpath("x:information", namespaces=namespaces)
        if len(information):
            transaction.job_state = VoipJobCarrierStates.objects.using("voipstack").get(
                id=3
            )
            transaction.save(using="voipstack")

        confirmation = qsc_xml_root.xpath("x:confirmation", namespaces=namespaces)
        if len(confirmation):
            transaction.job_state = VoipJobCarrierStates.objects.using("voipstack").get(
                id=7
            )
            transaction.save(using="voipstack")

        completion = qsc_xml_root.xpath("x:completion", namespaces=namespaces)
        if len(completion):
            transaction.job_state = VoipJobCarrierStates.objects.using("voipstack").get(
                id=4
            )
            transaction.save(using="voipstack")

        contractidentanswer = qsc_xml_root.xpath(
            "x:contractidentanswer", namespaces=namespaces
        )
        if len(contractidentanswer):
            transaction.job_state = VoipJobCarrierStates.objects.using("voipstack").get(
                id=4
            )
            transaction.save(using="voipstack")

            # write WBCI data to contract
            xml_dict = xmltodict.parse(b64decode(xml).decode())
            response_numbers = xml_dict["xmlanswer"]["contractidentanswer"]["voipinfo"]

            if response_numbers is None:
                numbers = VoipMdatNumbers.objects.filter(
                    reference_code=referenceNoPartner[0].text
                )

                for number in numbers:
                    number.status = VoipMdatNumbers.NUM_STATUS_DISABLED
                    number.save(using="voipstack")
            else:
                # ['number']
                element_number = response_numbers["number"]

                number_qry = VoipMdatNumbers.objects.filter(country_code=49)

                if "onkz" in element_number:
                    number_qry = number_qry.filter(
                        national_code=int(element_number["onkz"])
                    )
                if "cli" in element_number:
                    if "blockFrom" in element_number:
                        text_len = len(element_number["blockFrom"])
                        number_qry = number_qry.filter(
                            subscriber_num=int(element_number["cli"][:-text_len])
                        )
                    else:
                        number_qry = number_qry.filter(
                            subscriber_num=int(element_number["cli"])
                        )
                # if 'tnb' in element_number:
                #    number = number.filter(national_code=int(element_number['onkz']))
                if "blockFrom" in element_number:
                    number_qry = number_qry.filter(
                        range_start=int(element_number["blockFrom"])
                    )
                if "blockTo" in element_number:
                    number_qry = number_qry.filter(
                        range_end=int(element_number["blockTo"])
                    )

                if len(number_qry) > 1:
                    pprint("TOO MANY RESULTS")
                elif len(number_qry) < 1:
                    pprint("NUMBER NOT FOUND")
                    number = VoipMdatNumbers(
                        customer=MdatCustomers.objects.using("voipstack").get(
                            pk=100000
                        ),
                        reference_code=referenceNoPartner[0].text,
                        carrier_code=VoipMdatCarrierCodes.objects.using(
                            "voipstack"
                        ).get(pk=1),
                        country_code=49,
                        national_code=int(element_number["onkz"]),
                        status=VoipMdatNumbers.NUM_STATUS_ACTIVE,
                    )
                    if "blockFrom" in element_number:
                        text_len = len(element_number["blockFrom"])
                        number.range_start = int(element_number["blockFrom"])
                        number.range_end = int(element_number["blockTo"])
                        number.subscriber_num = int(element_number["cli"][:-text_len])
                    else:
                        number.subscriber_num = int(element_number["cli"])
                    number.save(force_insert=True)
                elif len(number_qry) == 1:
                    number = number_qry.first()
                    number.reference_code = referenceNoPartner[0].text
                    number.save()
                else:
                    pprint("ERROR DURING NUMBER SEARCH")

        try:
            latest_line = (
                VoipJobLog.objects.using("voipstack")
                .filter(job=transaction)
                .latest("log_row")
            )
            line_num = latest_line.log_row
        except VoipJobLog.DoesNotExist:
            line_num = 0

        log_entity_in = VoipJobLog(
            job=transaction,
            log_row=line_num + 1,
            log_text=b64decode(xml).decode(),
            date_added=datetime.now(),
            carrier_id=100,
            status_code=200,
        )
        log_entity_in.save(using="voipstack", force_insert=True)

        return {"successful": "true"}


SubmitService.event_manager.add_listener(
    "method_return_string", on_method_return_string
)

app = Application(
    [SubmitService],
    "https://partner.qsc.de/xmlgw/submit",
    out_protocol=Soap11(),
    in_protocol=Soap11(validator="soft"),
)

hello_world_service = csrf_exempt(DjangoApplication(app))
