from django.urls import path
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoView

from django_ispstack_api_plusnet_vc.django_ispstack_api_plusnet_vc.views import (
    SubmitService,
)

urlpatterns = [
    path(
        "submit",
        DjangoView.as_view(
            services=[SubmitService],
            tns="https://partner.qsc.de/xmlgw/submit",
            in_protocol=Soap11(validator="soft"),
            out_protocol=Soap11(),
        ),
    ),
]
