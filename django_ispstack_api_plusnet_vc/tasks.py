# TODO: This file needs major refactoring, code quality is low

from pprint import pprint

import requests
from Crypto.Hash import SHA1
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from celery import shared_task
from django.db import transaction
from lxml import etree
from zeep import Client
from zeep.transports import Transport

from django_voipstack_actions.django_voipstack_actions.models import *


def sign_data(private_key, data):
    """
    param: private_key_loc Path to your private key
    param: package Data to be signed
    return: base64 encoded signature
    """

    key = RSA.importKey(private_key)
    h = SHA1.new(data)
    signer = PKCS1_v1_5.new(key)
    signature = signer.sign(h)

    return signature


@shared_task(name="ispstack_api_plusnet_vc_send_transactions")
def ispstack_api_plusnet_vc_send_transactions():
    worker = IspstackApiPlusnetVcSendTransaction()
    worker.handle()

    return


class IspstackApiPlusnetVcSendTransaction:
    help = "Process Transactions for QSC"

    PLUSNET_NS = "http://partner.qsc.de/xchange/voip-4.0"

    namespaces = {"x": PLUSNET_NS}

    def handle(self, *args, **options):
        all_unprocessed_jobs = (
            VoipJob.objects.using("voipstack")
            .filter(job_state_id=1)
            .filter(action_id__in=(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
            .order_by("id")
        )

        for current_job in all_unprocessed_jobs:
            print("Transaction ID: " + str(current_job.id))

            partner_no = current_job.voipjoboptions_set.get(attribute__id=100).value
            reference_no_partner = current_job.voipjoboptions_set.get(
                attribute__id=101
            ).value

            xmlrequest = etree.Element("xmlrequest", nsmap={None: self.PLUSNET_NS})

            qheader = etree.Element("header")

            qhpartno = etree.Element("partnerno")
            qhpartno.text = partner_no
            qheader.append(qhpartno)

            qhrefnooart = etree.Element(
                "referenceNoPartner"
            )  # ein kunde kann mehrere referezen haben
            qhrefnooart.text = reference_no_partner  # QSC_VC_ReferenceNoPartner
            qheader.append(qhrefnooart)

            qhtransno = etree.Element("transactionNo")
            qhtransno.text = str(current_job.id)  # QSC_VC_TransactionNo
            qheader.append(qhtransno)

            qhtstamp = etree.Element("timestamp")
            timestamp_time = current_job.date_added.strftime("%d.%m.%Y %H:%M:%S")
            qhtstamp.text = str(timestamp_time)  # timestamp from create time of issue
            qheader.append(qhtstamp)

            xmlrequest.append(qheader)

            if current_job.action_id == 3 or current_job.action_id == 5:
                xmlrequest.append(self.qscVcNeuerVertragRufnummerEinzel(current_job))
            elif current_job.action_id == 4 or current_job.action_id == 6:
                xmlrequest.append(self.qscVcNeuerVertragRufnummerBlock(current_job))
            elif current_job.action_id == 14:
                xmlrequest.append(self.qscVcZusaetzlicheRufnummerEinzel(current_job))
            elif current_job.action_id == 15:
                xmlrequest.append(self.qscVcZusaetzlicheRufnummerBlock(current_job))
            elif current_job.action_id == 7:
                xmlrequest.append(self.qscVcStammdatenAendern(current_job))
            elif current_job.action_id == 8:
                xmlrequest.append(self.qscVcStorno(current_job))
            elif current_job.action_id == 9:
                xmlrequest.append(self.qscVcKuendigung(current_job))
            elif current_job.action_id == 10:
                xmlrequest.append(self.qscVcWbciPortVeroeffentlichen(current_job))
            elif current_job.action_id == 11:
                xmlrequest.append(self.qscVcWbciPortAbVerschieben(current_job))
            elif current_job.action_id == 12:
                xmlrequest.append(self.qscVcWbciPortAbLoeschen(current_job))
            elif current_job.action_id == 13:
                xmlrequest.append(self.qscVcWbciVertragsinfo(current_job))
            elif current_job.action_id == 16:
                xmlrequest.append(self.qscVcRufnummerAbbestellen(current_job))
            else:
                raise ValueError("Unknown Job :-(")

            xmlout = etree.tostring(
                xmlrequest,
                pretty_print=True,
                xml_declaration=True,
                encoding="UTF-8",
                standalone="yes",
            )

            try:
                latestline = (
                    VoipJobLog.objects.using("voipstack")
                    .filter(job=current_job)
                    .latest("log_row")
                )
                linenum = latestline.log_row
            except VoipJobLog.DoesNotExist:
                linenum = 0

            log_entity_out = VoipJobLog(
                job=current_job,
                log_row=linenum + 1,
                log_text=xmlout.decode(),
                date_added=datetime.now(),
                carrier_id=1,
                status_code=200,
            )
            log_entity_out.save(using="voipstack", force_insert=True)

            sendres = self.qscVcSendRequest(partner_no, xmlrequest)

            return_xml_root = etree.fromstring(sendres.xml)

            try:
                latestline = (
                    VoipJobLog.objects.using("voipstack")
                    .filter(job=current_job)
                    .latest("log_row")
                )
                linenum = latestline.log_row
            except VoipJobLog.DoesNotExist:
                linenum = 0

            log_entity_in = VoipJobLog(
                job=current_job,
                log_row=linenum + 1,
                log_text=sendres.xml.decode(),
                date_added=datetime.now(),
                carrier_id=100,
                status_code=200,
            )
            log_entity_in.save(using="voipstack", force_insert=True)

            acceptance = return_xml_root.xpath(
                "x:acceptance", namespaces=self.namespaces
            )
            if len(acceptance):
                current_job.job_state = VoipJobCarrierStates.objects.using(
                    "voipstack"
                ).get(id=2)
                current_job.save(using="voipstack")

            refusal = return_xml_root.xpath("x:refusal", namespaces=self.namespaces)
            if len(refusal):
                current_job.job_state = VoipJobCarrierStates.objects.using(
                    "voipstack"
                ).get(id=6)
                current_job.save(using="voipstack")

    def qscVcNeuerVertragRufnummerEinzel(self, transaction):
        qorder = etree.Element("order")

        qodemand = etree.Element("demand")

        transact_attr_productcode = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=1
        )

        qodproductcode = etree.Element("productcode")
        qodproductcode.text = transact_attr_productcode.value
        qodemand.append(qodproductcode)

        qoddesireddate = etree.Element("desiredDate")
        qoddesireddate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qodemand.append(qoddesireddate)

        qorder.append(qodemand)

        transact_attr_street = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=2
        )
        transact_attr_housenr = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=3
        )
        transact_attr_zipcode = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=4
        )
        transact_attr_city = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=5
        )
        transact_attr_onkz = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=9
        )
        transact_attr_quantity = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=10
        )
        transact_attr_routingprefix = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=11
        )

        qoinst = etree.Element("installation")

        qoistreet = etree.Element("street")
        qoistreet.text = transact_attr_street.value
        qoinst.append(qoistreet)

        qoihousenr = etree.Element("housenumber")
        qoihousenr.text = transact_attr_housenr.value
        qoinst.append(qoihousenr)

        qoizip = etree.Element("zip")
        qoizip.text = transact_attr_zipcode.value
        qoinst.append(qoizip)

        qoicity = etree.Element("city")
        qoicity.text = transact_attr_city.value
        qoinst.append(qoicity)

        qoilocationconfirmed = etree.Element("locationConfirmed")
        qoilocationconfirmed.text = "true"
        qoinst.append(qoilocationconfirmed)

        transact_personorcompany = (
            VoipJobOptions.objects.using("voipstack")
            .get(job=transaction, attribute_id=102)
            .value
        )

        if transact_personorcompany == "0":
            transact_attr_company = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=12
            )

            qoicompany = etree.Element("company")
            qoicompany.text = transact_attr_company.value
            qoinst.append(qoicompany)
        elif transact_personorcompany == "1":
            transact_attr_sal = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=6
            )
            transact_attr_fn = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=7
            )
            transact_attr_ln = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=8
            )

            qoiperson = etree.Element("person")

            qoipsal = etree.Element("salutation")
            qoipsal.text = transact_attr_sal.value
            qoiperson.append(qoipsal)

            qoipfn = etree.Element("firstname")
            qoipfn.text = transact_attr_fn.value
            qoiperson.append(qoipfn)

            qoipln = etree.Element("lastname")
            qoipln.text = transact_attr_ln.value
            qoiperson.append(qoipln)

            qoinst.append(qoiperson)
        else:
            raise ValueError("Wrong Input in transact_personorcompany!")

        qoidateofbirth = etree.Element("dateOfBirth")
        qoidateofbirth.text = "01.01.2000"
        qoinst.append(qoidateofbirth)

        qorder.append(qoinst)

        qovoip = etree.Element("voip")

        qovonkz = etree.Element("onkz")
        qovonkz.text = str(int(transact_attr_onkz.value))
        qovoip.append(qovonkz)

        qovquantity = etree.Element("quantity")
        qovquantity.text = str(int(transact_attr_quantity.value))
        qovoip.append(qovquantity)

        qovrp = etree.Element("routingPrefix")
        qovrp.text = transact_attr_routingprefix.value
        qovoip.append(qovrp)

        if transaction.action_id == 5:
            qovport = etree.Element("porting")

            transact_attr_tnb_old = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=22
            )

            transact_attr_canc_rem_num = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=23
            )

            transact_attr_wbci_id = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=21
            )

            transact_attr_clis = (
                VoipJobOptions.objects.using("voipstack")
                .filter(job=transaction)
                .filter(attribute_id=14)
            )

            qovplist = etree.Element("numList")

            for cli in transact_attr_clis:
                qovplcli = etree.Element("cli")
                qovplcli.text = cli.value
                qovplist.append(qovplcli)

            qovport.append(qovplist)

            qovptnb = etree.Element("tnb")
            qovptnb.text = transact_attr_tnb_old.value + "-000"
            qovport.append(qovptnb)

            qovpcrm = etree.Element("cancelRemainingMSN")
            qovpcrm.text = transact_attr_canc_rem_num.value
            qovport.append(qovpcrm)

            qovpwbci = etree.Element("wbci")
            qovpwbciid = etree.Element("wbciId")
            qovpwbciid.text = transact_attr_wbci_id.value
            qovpwbci.append(qovpwbciid)
            qovport.append(qovpwbci)

            qovoip.append(qovport)

        qorder.append(qovoip)

        return qorder

    def qscVcNeuerVertragRufnummerBlock(self, transaction):
        # if transaction.u_personorcompany != 0:
        #    self.stdout.write(self.style.SUCCESS('Wrong Input in u_personorcompany, only company allowed!'))
        #    exit(500)

        qorder = etree.Element("order")

        qodemand = etree.Element("demand")

        transact_attr_productcode = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=1
        )

        qodproductcode = etree.Element("productcode")
        qodproductcode.text = transact_attr_productcode.value
        qodemand.append(qodproductcode)

        qoddesireddate = etree.Element("desiredDate")
        qoddesireddate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qodemand.append(qoddesireddate)

        qorder.append(qodemand)

        transact_attr_street = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=2
        )
        transact_attr_housenr = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=3
        )
        transact_attr_zipcode = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=4
        )
        transact_attr_city = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=5
        )
        transact_attr_onkz = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=9
        )
        transact_attr_block_size = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=10
        )
        transact_attr_routingprefix = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=11
        )
        transact_attr_company = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=12
        )

        qoinst = etree.Element("installation")

        qoistreet = etree.Element("street")
        qoistreet.text = transact_attr_street.value
        qoinst.append(qoistreet)

        qoihousenr = etree.Element("housenumber")
        qoihousenr.text = transact_attr_housenr.value
        qoinst.append(qoihousenr)

        qoizip = etree.Element("zip")
        qoizip.text = transact_attr_zipcode.value
        qoinst.append(qoizip)

        qoicity = etree.Element("city")
        qoicity.text = transact_attr_city.value
        qoinst.append(qoicity)

        qoilocationconfirmed = etree.Element("locationConfirmed")
        qoilocationconfirmed.text = "true"
        qoinst.append(qoilocationconfirmed)

        qoicompany = etree.Element("company")
        qoicompany.text = transact_attr_company.value
        qoinst.append(qoicompany)

        qoidateofbirth = etree.Element("dateOfBirth")
        qoidateofbirth.text = "01.01.2000"
        qoinst.append(qoidateofbirth)

        qorder.append(qoinst)

        qovoip = etree.Element("voip")

        qovonkz = etree.Element("onkz")
        qovonkz.text = str(int(transact_attr_onkz.value))
        qovoip.append(qovonkz)

        if transaction.action_id in (4, 6):
            qovblocksize = etree.Element("blockSize")
            qovblocksize.text = str(int(transact_attr_block_size.value))
            qovoip.append(qovblocksize)
        else:
            qovquan = etree.Element("quantity")
            qovquan.text = str(int(transact_attr_block_size.value))
            qovoip.append(qovquan)

        qovrp = etree.Element("routingPrefix")
        qovrp.text = transact_attr_routingprefix.value
        qovoip.append(qovrp)

        if transaction.action_id == 6:
            qovport = etree.Element("porting")

            transact_attr_tnb_old = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=22
            )

            transact_attr_canc_rem_num = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=23
            )

            transact_attr_wbci_id = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=21
            )

            transact_attr_block_cli = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=24
            )

            transact_attr_block_main = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=25
            )

            transact_attr_block_from = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=26
            )

            transact_attr_block_to = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=27
            )

            block_length = len(transact_attr_block_to.value)

            qovpblock = etree.Element("numBlock")

            qovpbcli = etree.Element("cli")
            qovpbcli.text = transact_attr_block_cli.value
            qovpblock.append(qovpbcli)

            qovpbmain = etree.Element("main")
            qovpbmain.text = transact_attr_block_main.value
            qovpblock.append(qovpbmain)

            qovpbfrom = etree.Element("from")
            qovpbfrom.text = transact_attr_block_from.value.zfill(block_length)
            qovpblock.append(qovpbfrom)

            qovpbto = etree.Element("to")
            qovpbto.text = transact_attr_block_to.value
            qovpblock.append(qovpbto)

            qovport.append(qovpblock)

            qovptnb = etree.Element("tnb")
            qovptnb.text = transact_attr_tnb_old.value + "-000"
            qovport.append(qovptnb)

            qovpcrm = etree.Element("cancelRemainingMSN")
            qovpcrm.text = transact_attr_canc_rem_num.value
            qovport.append(qovpcrm)

            qovpwbci = etree.Element("wbci")
            qovpwbciid = etree.Element("wbciId")
            qovpwbciid.text = transact_attr_wbci_id.value
            qovpwbci.append(qovpwbciid)
            qovport.append(qovpwbci)

            qovoip.append(qovport)

        qorder.append(qovoip)

        return qorder

    def qscVcZusaetzlicheRufnummerEinzel(self, transaction):
        qava = etree.Element("addVoipAccount")

        qavadesireddate = etree.Element("changeDate")
        qavadesireddate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qava.append(qavadesireddate)

        qava.append(qavadesireddate)

        transact_attr_quantity = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=10
        )

        qavaquantity = etree.Element("quantity")
        qavaquantity.text = str(int(transact_attr_quantity.value))
        qava.append(qavaquantity)

        return qava

    def qscVcZusaetzlicheRufnummerBlock(self, transaction):
        qava = etree.Element("addVoipAccount")

        qavadesireddate = etree.Element("changeDate")
        qavadesireddate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qava.append(qavadesireddate)

        transact_attr_quantity = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=10
        )

        qavablocksize = etree.Element("blockSize")
        qavablocksize.text = str(int(transact_attr_quantity.value))
        qava.append(qavablocksize)

        return qava

    def qscVcStammdatenAendern(self, transaction):
        qchangeContrData = etree.Element("changeContractData")

        qccddesireddate = etree.Element("changeDate")
        qccddesireddate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qchangeContrData.append(qccddesireddate)

        transact_attr_street = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=2
        )
        transact_attr_housenr = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=3
        )
        transact_attr_zipcode = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=4
        )
        transact_attr_city = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=5
        )
        transact_attr_country = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=16
        )

        qccdinst = etree.Element("installation")

        qccdistreet = etree.Element("street")
        qccdistreet.text = transact_attr_street.value
        qccdinst.append(qccdistreet)

        qccdihousenr = etree.Element("housenumber")
        qccdihousenr.text = transact_attr_housenr.value
        qccdinst.append(qccdihousenr)

        qccdizip = etree.Element("zip")
        qccdizip.text = transact_attr_zipcode.value
        qccdinst.append(qccdizip)

        qccdicity = etree.Element("city")
        qccdicity.text = transact_attr_city.value
        qccdinst.append(qccdicity)

        qccdicountry = etree.Element("country")
        qccdicountry.text = transact_attr_country.value
        qccdinst.append(qccdicountry)

        qccdilocationconfirmed = etree.Element("locationConfirmed")
        qccdilocationconfirmed.text = "true"
        qccdinst.append(qoilocationconfirmed)

        if transaction.u_personorcompany == 0:
            transact_attr_company = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=12
            )

            qccdicompany = etree.Element("company")
            qccdicompany.text = transact_attr_company.value
            qccdinst.append(qccdicompany)
        elif transaction.u_personorcompany == 1:
            transact_attr_sal = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=6
            )
            transact_attr_fn = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=7
            )
            transact_attr_ln = VoipJobOptions.objects.using("voipstack").get(
                job=transaction, attribute_id=8
            )

            qccdiperson = etree.Element("person")

            qccdipsal = etree.Element("salutation")
            qccdipsal.text = transact_attr_sal.value
            qccdiperson.append(qccdipsal)

            qccdipfn = etree.Element("firstname")
            qccdipfn.text = transact_attr_fn.value
            qccdiperson.append(qccdipfn)

            qccdipln = etree.Element("lastname")
            qccdipln.text = transact_attr_ln.value
            qccdiperson.append(qccdipln)

            qccdinst.append(qccdiperson)
        else:
            raise ValueError("Wrong Input in transact_personorcompany!")

        qccdidateofbirth = etree.Element("dateOfBirth")
        qccdidateofbirth.text = "01.01.2000"
        qccdinst.append(qoidateofbirth)

        qchangeContrData.append(qccdinst)

        return qchangeContrData

    def qscVcStorno(self, transaction):
        qstorno = etree.Element("storno")

        transact_attr_storno_tnr = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=17
        )

        transact_attr_storno_type = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=18
        )

        qstnr = etree.Element("stornoTransactionNo")
        qstnr.text = transact_attr_storno_tnr.value
        qstorno.append(qstnr)

        qstype = etree.Element("stornoType")
        qstype.text = transact_attr_storno_type.value
        qstorno.append(qstype)

        return qstorno

    def qscVcKuendigung(self, transaction):
        qtermination = etree.Element("termination")

        qtcanceldate = etree.Element("cancelDate")
        qtcanceldate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qtermination.append(qtcanceldate)

        transact_attr_reason = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=13
        )

        qtcancelreason = etree.Element("cancelReason")
        qtcancelreason.text = transact_attr_reason.value
        qtermination.append(qtcancelreason)

        return qtermination

    def qscVcWbciPortVeroeffentlichen(self, transaction):
        qwbciapproval = etree.Element("wbciapproval")

        transact_attr_wbciid = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=19
        )
        transact_attr_account_ids = (
            VoipJobOptions.objects.using("voipstack")
            .filter(job=transaction)
            .filter(attribute_id=15)
        )
        transact_attr_block_size = (
            VoipJobOptions.objects.using("voipstack")
            .filter(job=transaction)
            .filter(attribute_id=10)
        )
        transact_attr_tnb = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=20
        )

        qwawbciid = etree.Element("wbciId")
        qwawbciid.text = transact_attr_wbciid.value
        qwbciapproval.append(qwawbciid)

        qwacanceldate = etree.Element("canceldate")
        qwacanceldate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qwbciapproval.append(qwacanceldate)

        qwaalist = etree.Element("accountList")

        for accountid in transact_attr_account_ids:
            qdvalaccount = etree.Element("account")
            qdvalaaccountid = etree.Element("accountID")
            qdvalaaccountid.text = accountid.value
            qdvalaccount.append(qdvalaaccountid)

            if len(transact_attr_block_size) > 0:
                qdvalablocksize = etree.Element("blockSize")
                qdvalablocksize.text = str(int(transact_attr_block_size.first().value))
                qdvalaccount.append(qdvalablocksize)

            qwaalist.append(qdvalaccount)

        qwbciapproval.append(qwaalist)

        qwatnb = etree.Element("tnb")
        qwatnb.text = transact_attr_tnb.value + "-000"
        qwbciapproval.append(qwatnb)

        return qwbciapproval

    def qscVcWbciPortAbVerschieben(self, transaction):
        qwbcishift = etree.Element("wbcishift")

        transact_attr_wbciid = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=19
        )

        qwswbciid = etree.Element("wbciId")
        qwswbciid.text = transact_attr_wbciid.value
        qwbcishift.append(qwswbciid)

        qwscanceldate = etree.Element("canceldate")
        qwscanceldate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qwbcishift.append(qwscanceldate)

        return qwbcishift

    def qscVcWbciPortAbLoeschen(self, transaction):
        qwbciremoval = etree.Element("wbciremoval")

        transact_attr_wbciid = VoipJobOptions.objects.using("voipstack").get(
            job=transaction, attribute_id=19
        )

        qwrwbciid = etree.Element("wbciId")
        qwrwbciid.text = transact_attr_wbciid.value
        qwbciremoval.append(qwrwbciid)

        return qwbciremoval

    def qscVcWbciVertragsinfo(self, transaction):
        qwbcicontrident = etree.Element("contractidentquery")

        return qwbcicontrident

    def qscVcRufnummerAbbestellen(self, transaction):
        qdva = etree.Element("deleteVoipAccount")

        qdvadesireddate = etree.Element("changeDate")
        qdvadesireddate.text = transaction.desire_date.strftime("%d.%m.%Y")
        qdva.append(qdvadesireddate)

        transact_attr_account_ids = (
            VoipJobOptions.objects.using("voipstack")
            .filter(job=transaction)
            .filter(attribute_id=15)
        )

        qdvalist = etree.Element("accountList")

        for accountid in transact_attr_account_ids:
            qdvalaccount = etree.Element("account")
            qdvalaaccountid = etree.Element("accountID")
            qdvalaaccountid.text = accountid.value
            qdvalaccount.append(qdvalaaccountid)
            qdvalist.append(qdvalaccount)

        qdva.append(qdvalist)

        return qdva

    def qscVcSendRequest(self, partnerno, xmlrequest):
        plusnet_vc_proxy_host = (
            settings.PLUSNET_VC_API_PROXY_HOST
            + ":"
            + str(settings.PLUSNET_VC_API_PROXY_PORT)
        )

        proxies = {"http": plusnet_vc_proxy_host, "https": plusnet_vc_proxy_host}

        rsession = requests.session()
        rsession.proxies.update(proxies)

        transport = Transport(session=rsession)

        client = Client(
            "https://" + settings.PLUSNET_VC_API_HOST + "/wsxmlService/submit?wsdl",
            transport=transport,
        )

        xml_pretty_string = etree.tostring(
            xmlrequest,
            pretty_print=True,
            xml_declaration=True,
            encoding="UTF-8",
            standalone="yes",
        )

        plusnet_rsa_sign_key = VoipMdatCarrierVars.objects.using("voipstack").get(
            name="plusnet_rsa_sign_key"
        )

        xml_signature = sign_data(plusnet_rsa_sign_key.value, xml_pretty_string)

        # with client.options(raw_response=True):
        #     result = client.service.submitXmlMessage(partnerno=partnerno, xml=xml_pretty_string,
        #                                              signature=xml_signature)

        pprint(xml_pretty_string)
        with client.settings(strict=False):
            result = client.service.submitXmlMessage(
                partnerno=partnerno, xml=xml_pretty_string, signature=xml_signature
            )

        return result


@shared_task(name="ispstack_api_plusnet_vc_sync_numbers")
def ispstack_api_plusnet_vc_sync_numbers():
    contracts = (
        VoipJobOptions.objects.using("voipstack")
        .filter(attribute_id=101)
        .values_list("value", flat=True)
        .distinct()
    )

    for contract in contracts:
        with transaction.atomic():
            new_transact = VoipJob(
                action_id=13,
                user_id=1,
                job_state_id=1,
            )

            new_transact.save(force_insert=True, using="voipstack")

            partner_no = VoipJobOptions(
                job=new_transact, attribute_id=100, value=str(6676394)
            )

            partner_no.save(force_insert=True, using="voipstack")

            reference_no_partner = VoipJobOptions(
                job=new_transact, attribute_id=101, value=contract
            )

            reference_no_partner.save(force_insert=True, using="voipstack")

    return
