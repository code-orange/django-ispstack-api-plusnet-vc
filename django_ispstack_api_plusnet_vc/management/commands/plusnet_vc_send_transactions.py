from django.core.management.base import BaseCommand

from django_ispstack_api_plusnet_vc.django_ispstack_api_plusnet_vc.tasks import (
    ispstack_api_plusnet_vc_send_transactions,
)


class Command(BaseCommand):
    help = "Process Transactions for QSC"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        return ispstack_api_plusnet_vc_send_transactions.apply()
