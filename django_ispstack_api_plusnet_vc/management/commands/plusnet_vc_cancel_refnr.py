from datetime import date
from datetime import datetime

from django.core.management.base import BaseCommand

from dit_enterprisehub_sap.models import *


class Command(BaseCommand):
    help = "refno cancel"

    def add_arguments(self, parser):
        parser.add_argument("txtid", type=int)

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        txtid = options["txtid"]

        old_transact = DolTelCeTransact.objects.using("sapserver").get(code=txtid)

        onnm_entity2 = Onnm.objects.using("sapserver").get(objectcode="DOL_TEL_CE_TA")
        onnm_entity2.refresh_from_db()  # make sure, AutoKey is Up2Date

        datenow = date.today()
        timenow = datetime.now()

        datecode = timenow.strftime("%Y%m%d")

        try:
            latestline = (
                DolTelCeTransact.objects.using("sapserver")
                .filter(code__contains=datecode)
                .latest("code")
            )
            transactno = int(latestline.code) + 1
        except DolTelCeTransact.DoesNotExist:
            transactno = int(timenow.strftime("%Y%m%d") + "000001")

        new_transact = DolTelCeTransact(
            code=transactno,
            docentry=onnm_entity2.autokey,
            canceled="N",
            object="DOL_TEL_CE_TA",
            usersign=4,
            transfered="N",
            createdate=datenow,
            createtime=timenow.strftime("%H%M"),
            updatedate=datenow,
            updatetime=timenow.strftime("%H%M"),
            datasource="I",
            u_partnerno=6676394,
            u_referencenopartner=old_transact.u_referencenopartner,
            u_desireddate=datenow,
            u_custnr=old_transact.u_custnr,
            u_personorcompany=old_transact.u_personorcompany,
            u_transactiontype=DolTelCeTaActn.objects.using("sapserver").get(code=7),
            u_transactionstate=DolTelCeTaState.objects.using("sapserver").get(code=1),
            u_carrier=DolTelCarrier.objects.using("sapserver").get(code=2),
            u_portnum=0,
        )

        onnm_entity2.autokey += 1
        onnm_entity2.save()

        new_transact.save(force_insert=True, using="sapserver")
        new_transact.refresh_from_db()

        transactline1 = DolTelCeTaLines(
            code=new_transact,
            lineid=1,
            object="DOL_TEL_CE_TA",
            u_transactattr=DolTelCeTaAttr.objects.using("sapserver").get(code=13),
            u_transactattrval="fristgerechte Kündigung",
        )
        transactline1.save(force_insert=True, using="sapserver")

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
