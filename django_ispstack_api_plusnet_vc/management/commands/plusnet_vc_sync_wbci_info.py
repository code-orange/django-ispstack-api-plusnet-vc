from django.core.management.base import BaseCommand

from django_ispstack_api_plusnet_vc.django_ispstack_api_plusnet_vc.tasks import (
    ispstack_api_plusnet_vc_sync_numbers,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        ispstack_api_plusnet_vc_sync_numbers()
